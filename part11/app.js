const path = require('path')

const express = require('express');
const { application } = require('express');

const app = express();

app.use(express.static('public'))

app.get('/', (req, res) => {
    const htmlFilePath = path.join(__dirname, 'views', 'index.html')
    res.sendFile(htmlFilePath)
});

app.get('/restaurants', (req, res) => {
    const htmlFilePath = path.join(__dirname, 'views', 'restaurants.html')
    res.sendFile(htmlFilePath)
});

app.get('/recommend', (req, res) => {
    const htmlFilePath = path.join(__dirname, 'views', 'recommend.html')
    res.sendFile(htmlFilePath)
});

app.get('/about', (req, res) => {
    const htmlFilePath = path.join(__dirname, 'views', 'about.html')
    res.sendFile(htmlFilePath)
});

app.listen(3000);