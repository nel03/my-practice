/*Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots
*/

let http = require('http');

let rooms = [
    {
        "roomNumber": 101,
        "description": "Two Rooms, 1 CR",
        "floor": "1st floor",
        "status": "Available"
    }
]

let port = 8000

let server = http.createServer(function(req, res){
    if(req.url == '/' && req.method == "GET"){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end(`Welcome to Bethany Homes`)
    } else if(req.url == '/rooms' && req.method == "GET"){
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(rooms))
        res.end(`Rooms Available`)
    } else if(req.url == '/addRooms' && req.method == "POST"){
        let request_body = ' '
            req.on('data', function(data){
                request_body += data
            });
            req.end('end', function(){
                console.log(typeof request_body);
                request_body = JSON.parse(request_body);

                let new_rooms = {
                    "roomNumber": request_body.roomNumber,
                    "description": request_body.description,
                    "floor": request_body.floor,
                    "status": request_body.status
                }
                rooms.push(new_rooms);
                console.log(rooms)

                res.writeHead(200, {'Content-Type': 'application/json'})
                res.write(JSON.stringify(new_rooms))
                res.end(`New Rooms were added`)
            });
    }else if(req.url == '/newUpdate' && req.method == "PUT"){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end(`Rooms were updated`);
    }else if(req.url == '/archive' && req.method == "DELETE"){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end(`Room is deleted`)
    }
});
