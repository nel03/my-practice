/*Create a fetch request using the GET method that will retrieve all the to do list items from JSON placeholder API */
/*Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console */
async function getToDoList(){
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");

    const reJson = await response.json();
    console.log(reJson)
    const myTitle =reJson.map((json) => 'title' + json.title)
    console.log(myTitle)
}
getToDoList()


/*GET method
Create a fetch request using the GET method that will retrieve a singe to do list item from JSON Placeholder API  */

fetch("https://jsonplaceholder.typicode.com/todos/26")
.then((response) => response.json())
.then((json) => console.log(json))

/*Provide the title and status
Using the data retrieved, print a message in the console that will provide the title and status of the do list item */
fetch("https://jsonplaceholder.typicode.com/todos/26")
.then((response) => response.json())
.then((json) => console.log(`title ${json.title} and Completed: ${json.completed}`))

/*POST method
Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API */
async function createToDoList(){
    const response = await fetch("https://jsonplaceholder.typicode.com/todos", {
        method: "POST",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            userId: 6,
            completed: false,
            title: "New Cars"
        })
    })
    const reJson = await response.json()
    console.log(reJson)
}
createToDoList()

/*PUT method
Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API */
/*Update a to do list item by changing the data structure to contain the following properties:
a. Title
B. Description
c. Status
d. Date Completed
e. User ID */
async function addToDoList(){
    const response = await fetch("https://jsonplaceholder.typicode.com/todos/23", {
        method: "PUT",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify({
            title: "Alfa Romeo",
            description: "The company has been involved in car racing since 1911",
            status: "In Progress",
            dateCompleted: "1910",
            userId: 6
        })
    })
    const reJson = await response.json();
    console.log(reJson);
}
addToDoList();

/*PATCH method
> Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API 
> Update a to do list item by changing the status to complete and add a date when the status was changed */
async function updateToDoList(){
    const response = await fetch("https://jsonplaceholder.typicode.com/todos/23", {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            status: 'Completed',
            dateCompleted: "1915"
        })
    })
    const reJson = await response.json();
    console.log(reJson);
}
updateToDoList();


/*DELETE method
Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API */

async function deleteToDoList(){
    const response = await fetch("https://jsonplaceholder.typicode.com/todos/23", {
        method: "DELETE"
    })
   const reJson = await response.json();
   console.log(reJson);
};
deleteToDoList();
