const mongoose = require('mongoose');

mongoose.connect(``, {
    newNewUrlParser: true,
    newUnifiedTopology: true
});

let dbConnect = mongoose.connect;

dbConnect.on('error', () => {
    console.error('Error Connection')
});
dbConnect.once('open', () => {
    console.log('Connected to MongoDB!')
});

module.exports = dbConnect;