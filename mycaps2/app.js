const express = require('express');
const dbConnect = require('./data/database');
const cors = require('cors')
const productRoutes = require('../routes/ProductRoutes');


const app = express();


const port = 5000;


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use('/users', userRoutes)
app.use('/products', productRoutes)

app.listen(port, () => console.log(`API is now online at port: ${port}`));