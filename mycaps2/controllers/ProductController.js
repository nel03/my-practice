const Product = require('../models/Product');
const auth = require('../auth');

async function addProduct(req,res){
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    if(!userData.isAdmin){
        return res.send('You are not an admin. You are not allowed to add Product');
    }

    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        stock: req.body.stock,
        size: req.body.size
    })
    try{
        const product = await newProduct.save();
        if(course){
            return res.send(`Product ${req.body.name} created`)
        }
        return res.send(false)
    }catch(error){
        console.log(error);
        return res.send(error)
    }
}

module.exports = {
    addProduct: addProduct
};