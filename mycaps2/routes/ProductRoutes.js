const express = require('express');
const router = express.Router();
const productController = require('../controllers/ProductController');
const auth = require('../auth')


router.post('/', auth.verify,ProductController.addProduct)

module.exports = router;