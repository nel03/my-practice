const server = http.createServer(function(req, res){
    res.writeHead(200, { 'Content-Type': 'text/html'})
    FileSystem.readFile('index.html', function(error, data){
        if (error){
            res.writeHead(404)
            res.write('Error: File Not Found')
        } else {
            res.write(data)
        }
        res.end()
    })
})