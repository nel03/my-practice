let http = require('http')

let bethany = [
    {
        "prodId": 1001,
        "name": "Two Rooms",
        "description": "Two Rooms with kitchen and 1 CR",
        "price": 20000,
        "available": 12
    },
    {
        "prodId": 1002,
        "name": "Three Rooms",
        "description": "Three Rooms, 2 CR and kitchen",
        "price": 28000,
        "available": 10
    },
    {
        "prodId": 1003,
        "name": "Transient Rooms",
        "description": "Per day",
        "price": 1500,
        "available": 5
    }
];

let port = 8000;

let server = http.createServer (function(req, res, next){
    if(req.url == '/' && req.method == 'GET'){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end(`Welcome to Bethany Homes`)
    } else if(req.url == '/bethany' && req.method == 'GET'){
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(bethany))
        res.end(`Rooms available`)
    } else if(req.url == '/store' && req.method == 'POST'){
        let request_body = ' '
        req.on('data', function(data){
            request_body += data
        })
        req.on('end', function(){
            console.log(typeof request_body)
            request_body = JSON.parse(request_body)

            let grocery = {
                "name": request_body.name,
                "size": request_body.size,
                "price": request_body.price,
                "stocks": request_body.stocks
            }
            bethany.push(grocery)
            console.log(bethany)

            res.writeHead(200, {'Content-Type': 'application/json'})
            res.write(JSON.stringify(grocery))
            res.end(`Grocery available at bethany Admin`)
        })
    } else if(req.url == '/upDateRooms' && req.method == 'PUT'){
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end(`Rooms and Grocery were updated`)
    }
})