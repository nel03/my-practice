const Product =  require('../models/product.model')

async function getCart(req, res){
    let product;
    try {
        product = await Product.findById(re.body.productId);
    } catch(error){
        next(error);
        return;
    }

    const cart = res.locals.cart;

    cart.addItem(product);
    req.session.cart = cart;

    res.status(201).json({
        message: "Cart Updated",
        newTotalItems: cart.totalQuality
})
}

function updateCartItem(req,res){
    const cart = res.locals.cart

    const updatedItemData = cart.updateItem(
        req.body.productId,
        +req.body.quantity
    );

    req.session.cart = cart;

    res.json({
        message: "Item updated!",
        updateCartData: {
            newTotalQuantity: cart.totalQuantity,
            newTotalPrice: cart.totalPrice,
            updatedItemPrice: updatedItemData.updatedItemPrice
        }
    });
}
