const Product = require('./product.model');

class Cart {
    constructor(items = [], totalQuantity = 0, totalPrice = 0){
        this.items = items;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    async updatePrices() {
        const productIds = this.items.map(function(item){
            return item.product.id
        });

        const products = await Product.findMultiple(productIds);

        const deletableCartItemProductIds = [];

        for (const cartItem of this.items){
            const product = products.find(function(prod){
                return prod.id === cartItem.product.id;
            });

            if (!product){
                // product was deleted!
                //"schedule" for removal from cart
                deletableCartItemProductIds.push(cartItem.product.id);
                continue;
            }
            // product was not deleted
            // set product data and total price to latest price from database
            cartItem.product = product;
            cartItem.totalPrice = cartItem.quantity * cartItem.product.price;
        }

        if (deletableCartItemProductIds.length > 0) {
            this.items = this.items.filter(function(item){
                return deletableCartItemProductIds.indexOf(item.product.id) < 0;
            });
        }

        // re-calculate cart totals
        this.totalQuantity = 0;
        this.totalPrice = 0

    }
}