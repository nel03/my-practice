//Files System package
const fs = require('fs');
const path = require('path');

// Express Setup
const { application } = require('express');
const express = require('express');

//Setup Application
const app = express();

//middleware
app.use(express.urlencoded({extended: false}));



app.get('/', (req, res) => {
    res.send('<form action="/store-user" method="POST"><label>Your Name</label><input type="text" name="username"><button>Submit</button></form>')
});

app.post('/store-user', (req, res) => {
    const userName = req.body.username;
    
    const filePath = path.join(__dirname, 'data', 'users.json');

    const fileData = fs.readFileSync(filePath);

    const existingUsers = JSON.parse(fileData);

    existingUsers.push(userName);

    fs.writeFileSync(filePath, JSON.stringify(existingUsers))

    res.send('Username Stored!')
});


app.get('/currentTime', (req, res) => {
    res.send(new Data().toISOString())
});

app.get('/users', (req, res) => {
    const filePath = path.join(__dirname, 'data', 'users.json');

    const fileData = fs.readFileSync(filePath);

    const existingUsers = JSON.parse(fileData);

    let responseData = '<ul>';

    for (const user of existingUsers){
        responseData += '<li>' + user + '</li>';
    }

    responseData += '</ul>';

    res.send(existingUsers);
})

app.listen(3000);

